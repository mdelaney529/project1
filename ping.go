package main

import (
	"fmt"
	"gitlab.com/akita/akita"
)

type PingMsg struct {
	akita.MsgMeta
	SeqID int
	Inport akita.Port
}

func (p *PingMsg) Meta() *akita.MsgMeta {
	return &p.MsgMeta
}

type PingRsp struct {
	akita.MsgMeta
	SeqID int
	Inport akita.Port
}

func (p *PingRsp) Meta() *akita.MsgMeta {
	return &p.MsgMeta
}
type StartPingEvent struct {
	*akita.EventBase
	Dst akita.Port
}

type RspPingEvent struct {
	*akita.EventBase
	pingMsg *PingMsg
}


type TickingPingAgent struct {
	*akita.TickingComponent

	InPort akita.Port
	OutPort akita.Port

	startTime           []akita.VTimeInSec
	numPingNeedToSend   int
	nextSeqID           int
	pingDst             akita.Port
}

func NewTickingPingAgent(
	name string,
	engine akita.Engine,
	freq akita.Freq,
) *TickingPingAgent {
	a := &TickingPingAgent{}
	a.TickingComponent = akita.NewTickingComponent(name, engine, freq, a)
	a.OutPort = akita.NewLimitNumMsgPort(a, 4, a.Name()+".OutPort")
	a.InPort = akita.NewLimitNumMsgPort(a, 4, a.Name()+".InPort")
	return a
}

func (a *TickingPingAgent) Tick(now akita.VTimeInSec) bool {
	madeProgress := false
	fmt.Printf("Tick\n")

	madeProgress = a.processInput(now) || madeProgress
	//madeProgress = a.sendRsp(now) || madeProgress
	//madeProgress = a.sendPing(now) || madeProgress
	return madeProgress
}

func (a *TickingPingAgent) processInput(now akita.VTimeInSec) bool {
	msg := a.InPort.Peek()
	if msg == nil {
		return false
	}
	fmt.Print("Processing Input\n")
	switch msg := msg.(type) {
	case *PingMsg:
		a.processingPingMsg(now, msg)
	case *PingRsp:
		a.processingPingRsp(now, msg)
	default:
		panic("unknown message type")
	}

	return true
}
 var responses int

func (a *TickingPingAgent) processingPingMsg(
	now akita.VTimeInSec,
	ping *PingMsg,
){
	fmt.Printf("Ping %d received at %s inport\n" , ping.SeqID, a.TickingComponent.ComponentBase.Name())
	a.InPort.Retrieve(now)
	r1 := RspPingEvent{
		EventBase: akita.NewEventBase(now, a),
		pingMsg: ping,
	}
	responses = 2
	a.Engine.Schedule(r1)
}

func (a *TickingPingAgent) processingPingRsp(
	now akita.VTimeInSec,
	ping *PingRsp,
){

	fmt.Printf("Ping Response %d received at %s inport\n", ping.SeqID, a.TickingComponent.ComponentBase.Name())
	a.InPort.Retrieve(now)
	responses--
	if (responses > 0){
		r2 := &PingRsp{
			SeqID: ping.SeqID+1,
		}
		r2.SendTime = now
		r2.Src = a.OutPort
		r2.Dst = ping.Inport
		a.OutPort.Send(r2)
	}


}

func (p *TickingPingAgent) Handle(e akita.Event) error {
	p.Lock()
	defer p.Unlock()
	switch e := e.(type) {
	case StartPingEvent:
		p.StartPing(e)
	case RspPingEvent:
		p.RspPing(e)
	default:
		panic("cannot handle event of type ")
	}
	return nil
}

func (a *TickingPingAgent) StartPing(evt StartPingEvent) {
	pingMsg := &PingMsg{
		SeqID: a.nextSeqID,
		Inport: a.InPort,
	}
	pingMsg.Src = a.OutPort
	pingMsg.Dst = evt.Dst
	pingMsg.SendTime = evt.Time()
	a.OutPort.Send(pingMsg)
	a.startTime = append(a.startTime, evt.Time())
	a.nextSeqID++
}

func (a *TickingPingAgent) RspPing(evt RspPingEvent) {
	msg := evt.pingMsg
	rsp := &PingRsp{
		SeqID: msg.SeqID,
		Inport: a.InPort,
	}
	rsp.SendTime = evt.Time()
	rsp.Src = a.OutPort
	rsp.Dst = msg.Inport
	a.OutPort.Send(rsp)
}

func main() {
	engine := akita.NewSerialEngine()
	agentA := NewTickingPingAgent("AgentA", engine, 1*akita.Hz)
	agentB := NewTickingPingAgent("AgentB", engine, 1*akita.Hz)
	conn := akita.NewDirectConnection("Conn", engine, 1*akita.GHz)

	conn.PlugIn(agentA.OutPort, 1)
	conn.PlugIn(agentB.OutPort, 1)
	conn.PlugIn(agentA.InPort, 1)
	conn.PlugIn(agentB.InPort, 1)


	e1 := StartPingEvent{
		EventBase: akita.NewEventBase(1, agentA),
		Dst: agentB.InPort,
	}
	agentA.Engine.Schedule(e1)

	engine.Run()

}